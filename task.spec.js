/*
 * task.spec.js
 *
 * Test file for your task class
 */
var expect, Task;

expect = require('./chai.js').expect;

Task = require('./task.js');

// ADD YOUR TESTS HERE
describe('Your makeNewTask function', function() {
   var task;

   task = Task.new();
   it('returns an object', function() {
      expect(task).to.be.a('object');
   });
});

// TEST PROTOTYPE METHODS
describe('Prototype methods:', function() {
   var task;

   beforeEach(function() {
      // This ensures every test sees a fresh empty map
      task = Task.new();
   });
   it('test **setTitle** prototype', function() {
      task.setTitle('title');
      expect(task.title).to.equal('title');
   });
   it('***check id*** should be 3 b/c this is the third time making a task', function() {
      expect(task.id).to.equal(5);
   });
   it('test **isCompleted** prototype where completed is null', function() {
      expect(task.isCompleted()).to.equal(false);
   });
   it('test **toggleCompleted** prototype where completed is null-> change to date ', function() {
      task.toggleCompleted();
      expect(task.completedTime).to.not.equal(null);
   });
   it('test **toggleCompleted** prototype where completed is not null-> change to null ', function() {
      task.toggleCompleted();
      task.toggleCompleted();
      expect(task.completedTime).to.equal(null);
   });
   it('test **isCompleted** prototype where completed is not null', function() {
      task.toggleCompleted();
      expect(task.isCompleted()).to.equal(true);
   });
   it('test **hasTag** prototype where tags empty', function() {
      expect(task.hasTag('tag')).to.equal(false);
   });
   it('test **addTag** prototype and **hasTag** when tags initally empty', function() {
      task.addTag('tag');
      expect(task.hasTag('tag')).to.equal(true);
   });
   it('test **removeTag** prototype', function() {
      task.addTag('tag');
      task.removeTag('tag');
      expect(task.hasTag('tag')).to.equal(false);
   });
   it('test **removeTag** prototype of tag not in array', function() {
      task.addTag('tag');
      task.removeTag('test');
      expect(task.hasTag('tag')).to.equal(true);
   });
   it('test **toggleTag** prototype of tag in array', function() {
      task.addTag('tag');
      task.toggleTag('tag');
      expect(task.hasTag('tag')).to.equal(false);
   });
   it('test **toggleTag** prototype of tag not in array', function() {
      task.toggleTag('tag');
      expect(task.hasTag('tag')).to.equal(true);
   });
   it('test **addTags** prototype of tags not in array', function() {
      var arr; 

      arr = ['one', 'two', 'three'];
      task.addTags(arr);
      expect(task.hasTag('one')).to.equal(true);
      expect(task.hasTag('two')).to.equal(true);
      expect(task.hasTag('three')).to.equal(true);
   });
   it('test **addTags** prototype of tags w/ some already in array', function() {
      var arr; 

      arr = ['one', 'two', 'three'];
      task.addTag('one');
      expect(task.hasTag('one')).to.equal(true);
      task.addTags(arr);
      expect(task.hasTag('one')).to.equal(true);
      expect(task.hasTag('two')).to.equal(true);
      expect(task.hasTag('three')).to.equal(true);
   });
   it('test **removeTags** prototype remove some tags from array do to removeTags', function() {
      var arr, arrRemove; 

      arr = ['one', 'two', 'three'];
      task.addTags(arr);
      arrRemove = ['two', 'three'];
      task.removeTags(arrRemove);
      expect(task.hasTag('one')).to.equal(true);
      expect(task.hasTag('two')).to.equal(false);
      expect(task.hasTag('three')).to.equal(false);
   });
   it('test **toggleTags** prototype of tags w/ some already in array and some not', function() {
      var arr1, arr2; 

      arr1 = ['one', 'two', 'three'];
      arr2 = ['two', 'three', 'four'];
      task.addTags(arr1);
      task.toggleTags(arr2);
      expect(task.hasTag('one')).to.equal(true);
      expect(task.hasTag('two')).to.equal(false);
      expect(task.hasTag('three')).to.equal(false);
      expect(task.hasTag('four')).to.equal(true);
   });
   /*it('test **clone**', function() {
   	  var arr, taskClone;

   	  arr = ['one', 'two', 'three'];
      task.setTitle('title');
      task.toggleCompleted();
      task.addTags(arr);
      taskClone = task.clone();
      expect(taskClone.title).to.equal('title');
      expect(taskClone.isCompleted()).to.equal(true);
      expect(taskClone.hasTag('one')).to.equal(true);
      expect(taskClone.hasTag('two')).to.equal(true);
      expect(taskClone.hasTag('three')).to.equal(true);
   });*/
});

//OTHER CONSTRUCTOR TESTS
describe('Your makeTaskFromObject function', function() {
   var task, obj;

   obj = {
   	  title: 'title',
   	  tags: ['one', 'two', 'three']
   };
   task = Task.fromObject(obj);
   it('returns an task based on object', function() {
   	  expect(task).to.be.a('object');
      expect(task.title).to.equal('title');
      expect(task.id).to.equal(2);
      expect(task.hasTag('one')).to.equal(true);
      expect(task.hasTag('two')).to.equal(true);
      expect(task.hasTag('three')).to.equal(true);
   });
});
describe('Your makeTaskFromString function', function() {
   var task, str;

   str = ' hi there! #hottopic ';
   task = Task.fromString(str);
   it('returns an task based on string', function() {
   	  expect(task).to.be.a('object');
      expect(task.title).to.equal('hi there!');
      expect(task.id).to.equal(3);
      expect(task.hasTag('hottopic')).to.equal(true);
   });
});