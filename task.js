/*
 * task.js
 *
 * Contains implementation for a "task" "class"
 */

var Task, proto, count;

// Helper method. You should not need to change it.
// Use it in makeTaskFromString
function processString(s) {
   'use strict';
   var tags, title;

   tags = [];
   title = s.replace(/\s*#([a-zA-Z]+)/g, function(m, tag) {
      tags.push(tag);

      return '';
   });

   return { title: title, tags: tags };
}

/*
 *       Constructors
 */

count = 0;

function makeNewTask() {
   var o;

   count += 1;
   o = Object.create(proto);
   o.title = '';
   o.completedTime = null;

   Object.defineProperty(o, 'id', {
      enumerable: true,
      configurable: false,
      writable: false,
      value: count
   });
   Object.defineProperty(o, 'tags', {
      enumerable: false,
      configurable: false,
      writable: false,
      value: []
   });
   Object.preventExtensions(o);

   return o;
}

function makeTaskFromObject(o) {
   var task;

   task = Task.new();
   task.setTitle(o.title);
   task.addTags(o.tags);

   return task;
}

function makeTaskFromString(str) {
   var v, task;

   str = str.trimLeft();
   v = processString(str);
   task = Task.fromObject(v);

   return task;
}

/*
 *       Prototype / Instance methods
 */

proto = {
   //Add instance methods here

   //**setTitle**
   setTitle: function setTitle(s) {
      this.title = s.trim();

      return this;
   },

   //**isCompleted**
   isCompleted: function isCompleted() {
      if (this.completedTime === null) {
         return false;
      }

      return true;
   },

   //**toggleCompleted**
   toggleCompleted: function toggleCompleted() {
      if (this.completedTime !== null) {
         this.completedTime = null;
      } else {
         this.completedTime = new Date();
      }

      return this;
   },

   //**hasTag**
   hasTag: function hasTag(s) {
      if (this.tags.indexOf(s) > -1) {
         return true;
      }

      return false;
   },

   //**addTag**
   addTag: function addTag(s) {
      if (this.tags.indexOf(s) > -1) {
         return this;
      }
      this.tags.push(s);

      return this;
   },

   //**removeTag**
   removeTag: function removeTag(s) {
      var i;

      for (i = 0; i < this.tags.length; i += 1) {
         if (this.tags[i] === s) {
            this.tags.splice(i, 1);
         }
      }

      return this;
   },

   //**toggleTag**
   toggleTag: function toggleTag(s) {
      var index;

      if (this.tags.indexOf(s) > -1) {
         index = this.tags.indexOf(s);
         this.tags.splice(index, 1);

         return this;
      } else {
         this.tags.push(s);
      }

      return this;
   },

   //**addTags**
   addTags: function addTags(arr) {
      var i;

      for (i = 0; i < arr.length; i += 1) {
         if (this.tags.indexOf(arr[i]) === -1) {
            this.tags.push(arr[i]);
         }
      }

      return this;
   },

   //**removeTags**
   removeTags: function removeTags(arr) {
      var i, index;

      for (i = 0; i < arr.length; i += 1) {
         if (this.tags.indexOf(arr[i]) > -1) {
            index = this.tags.indexOf(arr[i]);
            this.tags.splice(index, 1);
         }
      }

      return this;
   },

   //**toggleTags**
   toggleTags: function toggleTags(arr) {
      var i, index;

      for (i = 0; i < arr.length; i += 1) {
         if (this.tags.indexOf(arr[i]) > -1) {
            index = this.tags.indexOf(arr[i]);
            this.tags.splice(index, 1);
         } else {
            this.tags.push(arr[i]);
         }
      }

      return this;
   },

   //**clone**
   /*clone: function clone() {
      var task, i;

      task = makeNewTask();
      task.title = this.title;
      task.completedTime = this.completedTime;
      for (i = 0; i < this.tags.length; i += 0) {
         task.tags.push(this.tags[i]);
      }
      task.id = this.id;

      return task;
   }*/

};

// DO NOT MODIFY ANYTHING BELOW THIS LINE
Task = {
   new: makeNewTask,
   fromObject: makeTaskFromObject,
   fromString: makeTaskFromString
};

Object.defineProperty(Task, 'prototype', {
   value: proto,
   writable: false
});

module.exports = Task;
